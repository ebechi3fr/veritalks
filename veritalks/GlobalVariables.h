//
//  GlobalVariables.h
//  veritalks
//
//  Created by Emeric Bechi on 4/23/15.
//  Copyright (c) 2015 Embesoft. All rights reserved.
//

#ifndef veritalks_GlobalVariables_h
#define veritalks_GlobalVariables_h

#define kApplicationName @"veritalks"
#define kTableName @"veritalks_db"
#define kSessionIdKey @"SessionId"
#define kBaseDspUrl @"http://dreamfactory.veritalks.bitnamiapp.com"
#define kUserEmail @"UserEmail"
#define kPassword @"UserPassword"
//#define kContainerName @"applications"
//#define kFolderName @"uploaded_files"

#endif
