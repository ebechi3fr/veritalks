//
//  Singleton.h
//  veritalks
//
//  Created by Emeric Bechi on 5/1/15.
//  Copyright (c) 2015 Embesoft. All rights reserved.
//

#import <Foundation/Foundation.h>
@import AVFoundation;

@interface Singleton : NSObject
@property (strong, nonatomic) AVPlayer *streamingPlayer;
@property (strong, nonatomic) NSTimer *timer;

+ (Singleton*)sharedSingleton;

@end
