//
//  TALKRecord.h
//  veritalks
//
//  Created by Emeric Bechi on 4/29/15.
//  Copyright (c) 2015 Embesoft. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TALKRecord : NSObject

@property(nonatomic,retain)NSString *record_Title;
@property(nonatomic,retain)NSString *record_Url;

@end
