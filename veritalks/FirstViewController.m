//
//  FirstViewController.m
//  veritalks
//
//  Created by Emeric Bechi on 4/21/15.
//  Copyright (c) 2015 Embesoft. All rights reserved.
//

#import "FirstViewController.h"
#import "SWGUserApi.h"
#import "TALKRecord.h"
#import "Singleton.h"

@import AVFoundation;

@interface FirstViewController ()

@property (strong, nonatomic) IBOutlet UITableView *veritalksTableView;
@property (strong, nonatomic) NSMutableArray *veritalksArray;
@property (strong, nonatomic) NSString *baseUrl;
@property (strong, nonatomic) NSString *currentTalkUrl;

@end

@implementation FirstViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.veritalksArray = [[NSMutableArray alloc] init];
    self.baseUrl = kBaseDspUrl;
    [self getVeritalksFromServer];
    
    self.veritalksTableView.delegate = self;
    self.veritalksTableView.dataSource = self;
}

-(void)viewDidDisappear:(BOOL)animated
{

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - TableView Delegate Methods

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.veritalksArray count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"veritalkCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    TALKRecord *record = [self.veritalksArray objectAtIndex:indexPath.row];
    [cell.textLabel setText:record.record_Title];
    return cell;
}

-(NSString *)setBaseUrlPath:(NSString*)baseUrl{
    NSString *lastPathComponent=[baseUrl lastPathComponent];
    NSString *basePath = nil;
    if(![lastPathComponent isEqualToString:@"rest"]){
        basePath=[baseUrl stringByAppendingString:@"/rest"];
    }
    else{
        basePath=baseUrl;
    }
    return basePath;
}

- (IBAction)logout:(id)sender {
    NIKApiInvoker *_api = [NIKApiInvoker sharedInstance];
    NSString  *baseDSPUrl= kBaseDspUrl;//[[NSUserDefaults standardUserDefaults] valueForKey:kBaseDspUrl];
    NSString  *swgSessionId=[[NSUserDefaults standardUserDefaults] valueForKey:kSessionIdKey];
    NSString *serviceName = @"user"; // your service name here
    NSString *apiName = @"session"; // rest path
    NSString *restApiPath = [NSString stringWithFormat:@"%@/%@/%@",baseDSPUrl,serviceName,apiName];
    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary* headerParams = [[NSMutableDictionary alloc] init];
    [headerParams setObject:kApplicationName forKey:@"X-DreamFactory-Application-Name"];
    [headerParams setObject:swgSessionId forKey:@"X-DreamFactory-Session-Token"];
    NSString* contentType = @"application/json";
    
    NSDictionary *requestBody;
    
    [_api dictionary:restApiPath method:@"POST" queryParams:queryParams body:requestBody headerParams:headerParams contentType:contentType completionBlock:^(NSDictionary *output, NSError *error) {
        NSLog(@"Error %@",error);

        [[NSUserDefaults standardUserDefaults] setValue:nil forKey:kSessionIdKey];
//        [[NSUserDefaults standardUserDefaults] setValue:nil forKey:kUserEmail];
//        [[NSUserDefaults standardUserDefaults] setValue:nil forKey:kPassword];
        
        [[NSUserDefaults standardUserDefaults] synchronize];
        dispatch_async(dispatch_get_main_queue(),^ (void){
            [self.navigationController popToRootViewControllerAnimated:YES];
        });
    }];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    TALKRecord *record = [self.veritalksArray objectAtIndex:indexPath.row];
    self.currentTalkUrl = record.record_Url;
    NSLog(@"currentURL: %@", self.currentTalkUrl);
    [self playTalkWithUrl:self.currentTalkUrl];
}

- (void)getVeritalksFromServer {
    NSString  *swgSessionId=[[NSUserDefaults standardUserDefaults] valueForKey:kSessionIdKey];
    if (swgSessionId.length>0) {
        NIKApiInvoker *_api = [NIKApiInvoker sharedInstance];
        NSString *serviceName = @"db"; // your service name here
        NSString *apiName = kTableName; // your table name
        NSString *restApiPath = [NSString stringWithFormat:@"%@/%@/%@",[self setBaseUrlPath:self.baseUrl],serviceName,apiName];
        NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
        queryParams[@"include_count"] = [NSNumber numberWithBool:TRUE];
        
        NSMutableDictionary* headerParams = [[NSMutableDictionary alloc] init];
        [headerParams setObject:kApplicationName forKey:@"X-DreamFactory-Application-Name"];
        [headerParams setObject:swgSessionId forKey:@"X-DreamFactory-Session-Token"];
        NSString* contentType = @"application/json";
        id bodyDictionary = nil;

        [_api dictionary:restApiPath method:@"GET" queryParams:queryParams body:bodyDictionary headerParams:headerParams contentType:contentType completionBlock:^(NSDictionary *responseDict, NSError *error) {
            NSLog(@"Error %@",error);
            dispatch_async(dispatch_get_main_queue(),^ (void){

            });
            if (error) {
                dispatch_async(dispatch_get_main_queue(),^ (void){
                    UIAlertView *message=[[UIAlertView alloc]initWithTitle:@"Error" message:error.localizedDescription delegate:nil cancelButtonTitle:@"ok" otherButtonTitles: nil];
                    [message show];
//                    [self.navigationController popToRootViewControllerAnimated:YES];
                });
                
            }else{
                [self.veritalksArray removeAllObjects];
                for (NSDictionary *recordInfo in [responseDict objectForKey:@"record"]) {
                    TALKRecord *newRecord=[[TALKRecord alloc]init];
                    [newRecord setRecord_Title:[recordInfo objectForKey:@"title"]];
                    [newRecord setRecord_Url:[recordInfo objectForKey:@"url"]];
                    [self.veritalksArray addObject:newRecord];
                }
                
                dispatch_async(dispatch_get_main_queue(),^ (void){
                    [self.veritalksTableView reloadData];
                    [self.veritalksTableView setNeedsDisplay];
                });
            }
            
        }];
        
        
    }else{
        
    }
}

- (void)playTalkWithUrl:(NSString *)url {
    AVPlayerItem *aPlayerItem = [[AVPlayerItem alloc] initWithURL:[NSURL URLWithString:url]];
    AVPlayer *anAudioStreamer = [[AVPlayer alloc] initWithPlayerItem:aPlayerItem];

    
    [Singleton sharedSingleton].streamingPlayer = anAudioStreamer;
    [[[Singleton sharedSingleton] streamingPlayer] play];
//    [self.streamingPlayer addObserver:self forKeyPath:@"status" options:0 context:nil];

    // Access Current Time
//    NSTimeInterval aCurrentTime = CMTimeGetSeconds(anAudioStreamer.currentTime);
    
    // Access Duration
//    NSTimeInterval aDuration = CMTimeGetSeconds(anAudioStreamer.currentItem.asset.duration);
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    if (object == [Singleton sharedSingleton].streamingPlayer && [keyPath isEqualToString:@"status"]) {
        if ([Singleton sharedSingleton].streamingPlayer.status == AVPlayerStatusFailed) {
            NSLog(@"AVPlayer Failed");
            
        } else if ([Singleton sharedSingleton].streamingPlayer.status == AVPlayerStatusReadyToPlay) {
            NSLog(@"AVPlayerStatusReadyToPlay");

            
        } else if ([Singleton sharedSingleton].streamingPlayer.status == AVPlayerItemStatusUnknown) {
            NSLog(@"AVPlayer Unknown");
            
        }
    }
}


@end
