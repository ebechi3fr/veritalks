//
//  SecondViewController.m
//  veritalks
//
//  Created by Emeric Bechi on 4/21/15.
//  Copyright (c) 2015 Embesoft. All rights reserved.
//

#import "SecondViewController.h"
#import "Singleton.h"

@interface SecondViewController ()

@property (strong, nonatomic) IBOutlet UILabel *timeElapsedLabel;
@property (strong, nonatomic) IBOutlet UILabel *totalTimelabel;
@property (strong, nonatomic) IBOutlet UISlider *currentTimeSlider;

@end

@implementation SecondViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    NSLog(@"SecondView did load!!");
    self.currentTimeSlider.minimumValue = 0.0f;
    self.currentTimeSlider.maximumValue = CMTimeGetSeconds([[Singleton sharedSingleton] streamingPlayer].currentItem.asset.duration);
    if ([[Singleton sharedSingleton] streamingPlayer].status == AVPlayerStatusReadyToPlay) {
        [self updateDisplay];
        [Singleton sharedSingleton].timer = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(timerFired:) userInfo:nil repeats:YES];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)playTrack:(UIButton *)sender {
    [[[Singleton sharedSingleton] streamingPlayer] play];
    [Singleton sharedSingleton].timer = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(timerFired:) userInfo:nil repeats:YES];
}

- (IBAction)pauseTrack:(UIButton *)sender {
    [[[Singleton sharedSingleton] streamingPlayer] pause];
    [[[Singleton sharedSingleton] timer] invalidate];
}

- (void)updateDisplay
{
    NSTimeInterval currentTime = CMTimeGetSeconds([[Singleton sharedSingleton] streamingPlayer].currentTime);
    self.currentTimeSlider.value = currentTime;
    [self updateSliderLabels];
}

- (void)updateSliderLabels
{
    NSTimeInterval currentTime = CMTimeGetSeconds([[Singleton sharedSingleton] streamingPlayer].currentTime);
    NSTimeInterval totalTime = self.currentTimeSlider.maximumValue;
    self.totalTimelabel.text = [NSString stringWithFormat:@"%.02f", totalTime];
    self.timeElapsedLabel.text = [NSString stringWithFormat:@"%.02f",currentTime];
}

- (IBAction)currentTimerSliderValueChanged:(UISlider *)sender {
    if ([Singleton sharedSingleton].timer) {
        [[[Singleton sharedSingleton] timer] invalidate];
    }
    [self updateSliderLabels];
}

- (IBAction)currentTimerSliderTouchUpInside:(UISlider *)sender {
    [[[Singleton sharedSingleton] streamingPlayer] pause];

    [[Singleton sharedSingleton].streamingPlayer seekToTime:CMTimeMakeWithSeconds(self.currentTimeSlider.value, NSEC_PER_SEC)];
    
    [[[Singleton sharedSingleton] streamingPlayer] play];
    
}

- (void)timerFired:(NSTimer*)timer {
    [self updateDisplay];
}

@end
