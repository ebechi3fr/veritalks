//
//  LoginViewController.m
//  veritalks
//
//  Created by Emeric Bechi on 4/21/15.
//  Copyright (c) 2015 Embesoft. All rights reserved.
//

#import "LoginViewController.h"
#import "SWGUserApi.h"

@interface LoginViewController ()

@property (strong, nonatomic) IBOutlet UITextField *emailTextField;
@property (strong, nonatomic) IBOutlet UITextField *passwordTextField;
@property (strong, nonatomic) NSString *baseUrl;

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    NSString  *userEmail=[[NSUserDefaults standardUserDefaults] valueForKey:kUserEmail];
    NSString  *userPassword=[[NSUserDefaults standardUserDefaults] valueForKey:kPassword];
//    self.url = [[NSUserDefaults standardUserDefaults] valueForKey:kBaseDspUrl];
    self.baseUrl = kBaseDspUrl;
    if(userEmail.length >0 && userPassword.length >0){
        [self.emailTextField setText:userEmail];
        [self.passwordTextField setText:userPassword];
        [self getNewSessionWithEmail:userEmail Password:userPassword];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSString *)setBaseUrlPath:(NSString*)baseUrl{
    NSString *lastPathComponent=[baseUrl lastPathComponent];
    NSString *basePath = nil;
    if(![lastPathComponent isEqualToString:@"rest"]){
        basePath=[baseUrl stringByAppendingString:@"/rest"];
    }
    else{
        basePath=baseUrl;
    }
    return basePath;
}

- (IBAction)SubmitActionEvent:(UIButton *)sender {
    if(self.emailTextField.text.length>0 && self.passwordTextField.text.length>0) {
        [self.emailTextField resignFirstResponder];
        [self.passwordTextField resignFirstResponder];
        NIKApiInvoker *_api = [NIKApiInvoker sharedInstance];
        NSString *serviceName = @"user"; // your service name here
        NSString *apiName = @"session"; // rest path
        NSString *restApiPath = [NSString stringWithFormat:@"%@/%@/%@",[self setBaseUrlPath:self.baseUrl],serviceName,apiName];
        NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
        NSMutableDictionary* headerParams = [[NSMutableDictionary alloc] init];
        [headerParams setObject:kApplicationName forKey:@"X-DreamFactory-Application-Name"];
        NSString* contentType = @"application/json";
        
        NSDictionary *requestBody = @{@"email": self.emailTextField.text, @"password": self.passwordTextField.text};
        [_api dictionary:restApiPath method:@"POST" queryParams:queryParams body:requestBody headerParams:headerParams contentType:contentType completionBlock:^(NSDictionary *responseDict, NSError *error) {
            NSLog(@"Error %@",error);
            dispatch_async(dispatch_get_main_queue(),^ (void){
                if(responseDict){
                    NSString *SessionId = [responseDict objectForKey:@"session_id"];
//                    [[NSUserDefaults standardUserDefaults] setValue:[self setBaseUrlPath:self.baseUrl] forKey:kBaseDspUrl];
                    [[NSUserDefaults standardUserDefaults] setValue:SessionId forKey:kSessionIdKey];
                    [[NSUserDefaults standardUserDefaults] setValue:self.emailTextField.text forKey:kUserEmail];
                    [[NSUserDefaults standardUserDefaults] setValue:self.passwordTextField.text forKey:kPassword];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    [self performSegueWithIdentifier:@"ShowInitialView" sender:self];
                }else{
                    UIAlertView *message=[[UIAlertView alloc]initWithTitle:@"Error" message:error.localizedDescription delegate:nil cancelButtonTitle:@"ok" otherButtonTitles: nil];
                    [message show];
                }
            });
            
        }];
    }
    else {
        UIAlertView *message=[[UIAlertView alloc]initWithTitle:@"" message:@"Please fill the all entry first." delegate:nil cancelButtonTitle:@"ok" otherButtonTitles: nil];
        [message show];
    }
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

-(void)viewWillAppear:(BOOL)animated{
    NSString  *userEmail=[[NSUserDefaults standardUserDefaults] valueForKey:kUserEmail];
    NSString  *userPassword=[[NSUserDefaults standardUserDefaults] valueForKey:kPassword];
    
    if(userEmail.length >0 && userPassword.length >0 ){
        [self.emailTextField setText:userEmail];
        [self.passwordTextField setText:userPassword];
    }else{
        [self.emailTextField setText:@""];
        [self.passwordTextField setText:@""];
    }
}

-(void)getNewSessionWithEmail:(NSString*)email Password:(NSString*)password{
//    NSString *baseDspUrl=baseUrl;

    NIKApiInvoker *_api = [NIKApiInvoker sharedInstance];
    NSString *serviceName = @"user"; // your service name here
    NSString *apiName = @"session"; // rest path
    NSString *restApiPath = [NSString stringWithFormat:@"%@/%@/%@",[self setBaseUrlPath:self.baseUrl],serviceName,apiName];
    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary* headerParams = [[NSMutableDictionary alloc] init];
    [headerParams setObject:kApplicationName forKey:@"X-DreamFactory-Application-Name"];
    NSString* contentType = @"application/json";
    
    NSDictionary *requestBody = @{@"email": email, @"password": password};
    
    [_api dictionary:restApiPath method:@"POST" queryParams:queryParams body:requestBody headerParams:headerParams contentType:contentType completionBlock:^(NSDictionary *output, NSError *error) {
        NSLog(@"Error %@",error);
        NSLog(@"OutPut %@",[output objectForKey:@"id"]);
        dispatch_async(dispatch_get_main_queue(),^ (void){
            if(output){
                NSString *SessionId=[output objectForKey:@"session_id"];
//                [[NSUserDefaults standardUserDefaults] setValue:baseDspUrl forKey:kBaseDspUrl];
                [[NSUserDefaults standardUserDefaults] setValue:SessionId forKey:kSessionIdKey];
                [[NSUserDefaults standardUserDefaults] setValue:email forKey:kUserEmail];
                [[NSUserDefaults standardUserDefaults] setValue:password forKey:kPassword];
                [[NSUserDefaults standardUserDefaults] synchronize];
                [self performSegueWithIdentifier:@"ShowInitialView" sender:self];
            }else{
                UIAlertView *message=[[UIAlertView alloc]initWithTitle:@"Error" message:error.localizedDescription delegate:nil cancelButtonTitle:@"ok" otherButtonTitles: nil];
                [message show];
            }
        });        
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
