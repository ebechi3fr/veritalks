//
//  Singleton.m
//  veritalks
//
//  Created by Emeric Bechi on 5/1/15.
//  Copyright (c) 2015 Embesoft. All rights reserved.
//

#import "Singleton.h"

@implementation Singleton

static Singleton* _sharedSingleton = nil;

#pragma mark Singleton Methods
+(Singleton*)sharedSingleton
{
    @synchronized([Singleton class])
    {
        if (!_sharedSingleton)
            _sharedSingleton = [[self alloc] init];
        return _sharedSingleton;
    }
    return nil;
}

+(id)alloc
{
    @synchronized([Singleton class])
    {
        NSAssert(_sharedSingleton == nil, @"Attempted to allocate a second instance of a singleton.");
        _sharedSingleton = [super alloc];
        return _sharedSingleton;
    }
    return nil;
}

-(id)init {
    self = [super init];
    if (self != nil) {

    }
    return self;
}

@end
